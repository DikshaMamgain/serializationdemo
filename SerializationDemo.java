/*A program to demonstrate serialization for various different dependencies.
When there is a composition between two classes then both the classes should implement Serializable.
If superclass is serializable then subclass is automatically serializable and need not to implement Serializable.
If a superclass is not serializable then subclass can still be serialized
*/
import java.io.*;
import java.util.*;
import java.io.*;

class Comp implements Serializable
{
   int x;
   Comp(){
         System.out.println("Java Program");
         }
}

class Cal
{ 
   int m1,m2;
   Cal(int m1,int m2)
   {
        this.m1=m1;
        this.m2=m2;	
        int avg = (m1+m2)/2;	
        System.out.println("Marks1 : " + m1 + " ; Marks2 : " + m2 + "Average : "+ avg);  
		System.out.println();
   }  

}
class SerializableEx extends Cal implements Serializable
{
   int x;
   String n;
   Comp D;
   SerializableEx( int x, String n)
   {
       super(45,48);
	   this.x = x;
	   this.n = n;
	   System.out.println("Name : " + n + " ; Age : " + x );
	   D = new Comp();
	   
   }
} 
class SerializationDemo{  
 public static void main(String[] args){  
  try{    
  SerializableEx O1 =new SerializableEx(25, "Java");    
  FileOutputStream fout=new FileOutputStream("D:\\SerializableEx.ser");  
  ObjectOutputStream out=new ObjectOutputStream(fout);  
  out.writeObject(O1);  
  out.flush();   
  out.close();  
  System.out.println("successfully serialized...!!!");  
  }
  catch(NotSerializableException e)
  {
  System.out.println(e);
  }  
  catch(IOException e)
  {
  System.out.println(e);
  } 
  catch(Exception e)
  {
  System.out.println(e);
  }  
 }  
}